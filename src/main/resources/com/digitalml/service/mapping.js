{
  "firstName": firstName,
  "lastName": lastName,
  "birthPlace": birthPlace,
  "gender": gender,
  "ethnicity": ethnicity,
  "nationality": nationality,
  "dob": dob,
  "middleName": middleName,
  "language": language,
  "id": id
}